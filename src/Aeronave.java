
//Cria a classe Aeronave e implementa a Classe Imprimivel, Contavel e ordena com Comparable
public class Aeronave implements Imprimivel, Contavel,
		Comparable<Aeronave> {
	
	//Declara��o dos Atributos
	private String codigo;
	private String descricao;
	private int capacidade;
	private static int totalAeronaves=0;
	
	//M�todo Construtor
	public Aeronave(String codigo,String descricao,int cap){
		this.codigo=codigo;
		this.descricao=descricao;
		this.capacidade=cap;
		//Cada vez que adicionar uma aeronaves ele atribui mais 1 no total
		//de aeronaves
		totalAeronaves++;
	}
	
	//Cria um get para retornar os dados do codigo
	public String getCodigo(){
		return codigo;
	}
	//Cria um get para retornar os dados da descri��o
	public String getDescricao() {
		return descricao;
	}
	
	//Cria um get para retornar os dados da capacidade
	public int getCapacidade() {
		return capacidade;
	}
	
	//M�todo toString, que retorna os dados
	@Override
	public String toString() {
		return codigo + " - " + descricao
		+ " (" + capacidade + ")";
	}
	
	//Cria um m�todo para imprimir as informa��es ou retornalas usando o toString
	@Override
	public void imprimir() {
		System.out.println(toString());	
	}
	
	//Retorna o total de Aeronaves criadas
	@Override
	public int total() {
		return totalAeronaves;
	}
	
	//M�todo de ordena��o que compara por descri��o
	@Override
	public int compareTo(Aeronave o) {
		return descricao.compareTo(o.descricao);
	}
}
