import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.IOException;
import java.time.Duration;


public class App {

	//Declara a classe App como main
	public static void main(String args[]){
		
		//Declarando as classes gerenciadores
		GerenciadorCias gerCias	=new GerenciadorCias();
		GerenciadorAeroportos gerAero	=new GerenciadorAeroportos();
		GerenciadorAeronaves gerAvioes	=new GerenciadorAeronaves();
		GerenciadorRotas gerRotas	=new GerenciadorRotas();
	
		
		try {
			gerCias.carregaDados();
		} catch (IOException e) {
			System.out.println("Erro carregando dados de cias.");
			System.exit(1);
		}
		
		
		
		
		
		
	}
}
