
//Declara a classe Aeroporto e implemetna o Comparable
public class Aeroporto implements Comparable<Aeroporto> {
	//Declara atributos da classe
	private String codigo;
	private String nome;
	private Geo loc;

	//Cria o construtor
	public Aeroporto(String codigo, String nome, Geo loc) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
	}
	
	//Declara os Gets dos atributos
	public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}
	//Declara a compara��o por nome usando o compareTo 
	@Override
	public int compareTo(Aeroporto o) {
		return nome.compareTo(o.nome);
	}
	//M�todo toString
	@Override
	public String toString() {
		return codigo + " - " + nome + " - " + loc;
	}
	
	//Sugest�o, usar o m�todo Imprimir da Aeronave, ver em aula
	
	
}