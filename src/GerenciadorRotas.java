import java.util.ArrayList; 
import java.util.Comparator;
//import java.util.Collections; Foi substituido por outro m�todo
//Na ordena��o por cia


public class GerenciadorRotas {

	//Criando um Array de Rota
	private ArrayList<Rota>rotas;
	
	//Metodo construtor
	public GerenciadorRotas(){
		rotas=new ArrayList<>();
	}
	
	//Adicionar rota
	public void adiconar(Rota r){
		rotas.add(r);
	}
	
	//Cria um Array e lista todas as Rotas
	public ArrayList<Rota> listarTodas(){
		return new ArrayList<Rota>(rotas);
	}
	
	//Ver o tamanho do Array(Ver total de Rotas)
	public int totalRotas(){
		return rotas.size();
	}
	
	//Ordenar por Cia
	public void ordenaCia() {
		//"Collections.sort(rotas);"
		rotas.sort((Rota r1, Rota r2)
			-> r1.getCia().getNome().compareTo(r2.getCia().getNome()));
	}
	
	//Ordenar por origem
	public void ordenaOrigem() {
		rotas.sort( (Rota r1, Rota r2)
			-> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));
	}
	
	//Ordenar por origem e cia
	public void ordenaOrigemCia(){
		rotas.sort(Comparator.comparing((Rota r) -> r.getOrigem().getNome()).
				thenComparing(r -> r.getCia().getNome()));
	}
	
	//Metodo para buscar por origem
	public ArrayList<Rota> buscarOrigem(Aeroporto origem) {
		//Cria um Array Auxiliar chamado de lista 
		ArrayList<Rota> lista = new ArrayList<>();
		for(Rota r : rotas) {			
			//System.out.println(r.getOrigem().getCodigo());
			if(origem.getCodigo().equals(r.getOrigem().getCodigo()))
				//Caso o codigo seja igual ele armazena na nova lista
				lista.add(r);					
		}
		return lista;
	}
	
	//Metodo toString
	@Override
	public String toString() {		
		StringBuilder aux = new StringBuilder();
		for(Rota r: rotas)
			aux.append(r + "\n");			
		return aux.toString();
	}
	
	
	
	
}
