import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GerenciadorAeronaves {

	//Cria um Array de aeronaves
	private ArrayList<Aeronave>aeronaves;
	
	//Construtor e cria uma nova ArrayList
	public GerenciadorAeronaves() {
		aeronaves = new ArrayList<>();
	}
	
	//Ordena��o pelo codigo
	public void ordenaCodigo() {
		//Copiado os coment�rios mas rever o reversed*
		//aeronaves.sort( (Aeronave a1, Aeronave a2)
		//		-> a1.getCodigo().compareTo(a2.getCodigo()));
		//aeronaves.sort(Comparator.comparing(a -> a.getCodigo()));
		aeronaves.sort(Comparator.comparing(Aeronave::getCodigo).reversed());
	}
	
	//Ordena pela descricao
	public void ordenaDescricao(){
		Collections.sort(aeronaves);
	}
	
	//Adiciona uma aeronave
	public void adcionar(Aeronave av){
		aeronaves.add(av);
	}
	
	//Cria um novo array para listar todas as aeronaves
	public ArrayList<Aeronave>listarTodas(){
		return new ArrayList<Aeronave>(aeronaves);
	}
	
	//M�todo para buscar a Aeronave pelo codigo
	public Aeronave buscarCodigo(String codigo){
		if(codigo==null)return null;
		for(Aeronave av:aeronaves){
			if(av.getCodigo().equals(codigo))return av;
		}
		return null;
	}
	
	///Metodo construtor, que percorre todas as aeronaves
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Aeronave av: aeronaves)
			aux.append(av.toString()+"\n");
		return aux.toString();
	}
	
	
	
	
}
