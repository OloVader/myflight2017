import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenciadorCias {

	//Declara o Array de CiaAerea com nome de empresas
	private ArrayList<CiaAerea>empresas;
	
	//Cria um Array
	public GerenciadorCias(){
		empresas=new ArrayList<>();
	}
	
	//Adiciona uma nova emrpesa no Array
	public void adicionar(CiaAerea cia){
		empresas.add(cia);
	}
	
	////Retorna todo o Array de empresas
	public ArrayList<CiaAerea>listarTodos(){
		//Cria um novo Array de CiaAerea e recebe os dados do Array empresas
		return new ArrayList<CiaAerea>(empresas);
	}
	
	//Busca pelo codigo e o retorna 
	public CiaAerea buscarCodigo(String codigo){
		for(CiaAerea c:empresas){
			if(codigo.equals(c.getCodigo()))
			return c;
		}
		return null;//Caso n encontre
	}
	
	//Busca pelo nome e o retorna
	public CiaAerea buscarNome(String nome){
		for(CiaAerea c:empresas){
			if(nome.equals(c.getNome()))
				return c;
		}
		return null;//Caso n encontre 
		
	}
	
	
	public void carregaDados() throws IOException{
		
		Path path2 = Paths.get("airlanes.dat");
		try (BufferedReader br = Files.newBufferedReader(path2, Charset.defaultCharset())){
			String linha = null;
			
			while((linha = br.readLine() )!=null ){
				
				Scanner sc = new Scanner(linha).useDelimiter(";"); // separador � ;
				String header = sc.nextLine(); // pula cabe�alho
				String codigo,nome;
				codigo=sc.next();
				nome=sc.next();
			}
		}catch (IOException x){
			System.err.format("Erro de E/S: %s%n",x);
		}
	}
	
	
	
	
	
	
	
}
