import java.util.ArrayList;
import java.util.Collections;

public class GerenciadorAeroportos {

	
	//Declara um Array de Aeroporto e o chama de aeroportos
	private ArrayList<Aeroporto>aeroportos;
	
	//Cria um Array de Aeroportos
	public GerenciadorAeroportos(){
		aeroportos=new ArrayList<>();
	}
	
	//Adiciona um aeroporto
	public void adicionar(Aeroporto a){
		aeroportos.add(a);
	}
	
	//Listar todos os aeroportos
	public ArrayList<Aeroporto>listarTodos(){
		//Retorna um novo Array de Aeroporto que recebe aeroportos
		return new ArrayList<Aeroporto>(aeroportos);
	}
	
	//Ordena��o pelo nome
	public void ordenaNome(){
		Collections.sort(aeroportos);
	}
	
	//Busca pelo codigo
	public Aeroporto buscarCodigo(String codigo){
		//Percorre todo o Array de Aeroportos
		for(Aeroporto a:aeroportos){
			if(codigo.equals(a.getCodigo()))
				return a;
		}
		return null;
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Aeroporto a: aeroportos)
			aux.append(a + "\n");			
		return aux.toString();
	}
	
	
	
}
