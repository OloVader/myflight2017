import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;


public class VooEscalas extends Voo {
	
	//Cria um array de rotas, por ser escalas
	//Vai ter mais de 1 rota a prosseguir
	private ArrayList<Rota> rotas;
	
	//Construtor e Reutilizando o dataHora de Voo
	public VooEscalas(LocalDateTime datahora)
	{
		super(datahora);
		rotas = new ArrayList<>();		
	}
	
	//Metodo para adicionar rotas
	public void adicionarEscala(Rota r){
		rotas.add(r);
	}
	//retorna o tamanho do array de rotas para 
	//Demonstrar quantas escalas possui
	public int getTotalEscalas() { 
		return rotas.size();		
	}
	//M�todo toString 
	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		for(Rota r: rotas)
			aux.append("\n   "+r);
		return super.toString() + aux.toString();
	}
	//Retorna as rotas existente ou caso n tenha retorna null	@Override
	public Rota getRota() {
		if(rotas.size() > 0)
			return rotas.get(0);
		else
			return null;
	}
	//M�todo para calcular a dura��o e terminar o m�todo
	@Override
	public Duration getDuracao() {
		int acummin=0; 
		//Cria um for para percorrer o tamanho do Array
		//E acumula os minutos no acumminutos
		for(Rota r:rotas){
			//Usa o "r" como referencia para cada Rota no array
			double dist = r.getOrigem().getLocal()
					.distancia(r.getDestino().getLocal());
			double dur = dist / 805 + 0.5;
			int minutos = (int) (dur * 60);
			acummin=acummin+minutos;
		}
		
		/*
		//Cria um for para percorrer o tamanho do Array
		//E acumula os minutos no acumminutos 
		for(int i=0;i<rotas.size();i++){
			double dist = rotas[i].getOrigem().getLocal()
					.distancia(rotas.getDestino().getLocal());
			double dur = dist / 805 + 0.5;
			int minutos = (int) (dur * 60);
			acummin=acummin+minutos;
		}*/
		// TODO: passar por todas as escalas, acumulando
		// as distâncias...
		return Duration.ofMinutes(acummin); // calcular corretamente!
	}
}
