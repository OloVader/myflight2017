public class CiaAerea {

	private static int totalCias=0;
	private String codigo;
	private String nome;
	
	//Construtor da classe
	public CiaAerea(String codigo,String nome){
		this.codigo=codigo;
		this.nome=nome;
		totalCias++;
	}
	
	//Cria os gets da classe para retornar os dados
	//armazenados
	public static int getTotalCias(){
		return totalCias;
	}
	public String getCodigo(){
		return codigo;
	}
	public String getNome(){
		return nome;
	}
	//Classe sem toString, ver com Cohen
	
}
