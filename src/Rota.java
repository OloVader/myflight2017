
//Cria uma implementação de comapração de rotas na classe
public class Rota implements Comparable<Rota> {

	//Cria atributos
	private CiaAerea cia;
	private Aeroporto origem;
	private Aeroporto destino;
	private Aeronave aeronave;
	
	//Método Construtor
	public Rota(CiaAerea cia,Aeroporto origem,Aeroporto destino,Aeronave aeronave){
		this.cia=cia;
		this.origem=origem;
		this.destino=destino;
		this.aeronave=aeronave;
	}
	
	//Declarando Gets
	public CiaAerea getCia() {
		return cia;
	}
	public Aeroporto getDestino() {
		return destino;
	}
	public Aeroporto getOrigem() {
		return origem;
	}
	public Aeronave getAeronave() {
		return aeronave;
	}
	
	
	//Metodo de ordenacao que ordena pelo nome
	@Override
	public int compareTo(Rota o){
		return cia.getNome().compareTo(o.cia.getNome());
	}
	
	//Metodo construtor
	@Override
	public String toString() {
		return cia.getCodigo()+" - "
				+origem.getCodigo()+" -> "
				+destino.getCodigo();
	}
}
