//Importando a biblioteca do java e incluindo do pacote time dura��o e localdatetime
import java.time.Duration;
import java.time.LocalDateTime;


//Cria uma classe abstrata de Voo
public abstract class Voo {
	
	//Cria um estatus do voo
	public enum Status { CONFIRMADO,ATRASADO,CANCELADO};
	
	//Cria uma variavel de data e hora
	private LocalDateTime datahora;
	//cria uam variavel de Estatus do voo
	private Status status;
	
	//Construtor que recebe a data e hora e o estatus automaticamente para Confirmado
	public Voo(LocalDateTime datahora){
		this.datahora=datahora;
		this.status=Status.CONFIRMADO;
	}
	
	public Voo()
	{		
		// Chama construtor anterior aqui
		//Rever o significado dos numeros com o Cohen
		this(LocalDateTime.of(2016, 8, 12, 12, 0));		
	}
	
	//Cria um abstract da Rota e retorna Rota e dura��o
	public abstract Rota getRota();
	public abstract Duration getDuracao();
	
	
	//Cria os gets da clase   de  datahora e status
	public LocalDateTime getDatahora() {
		return datahora;
	}
	public Status getStatus() {
		return status;
	}
	
	//Cria o set para alterar o status
	public void setStatus(Status novo) {
		this.status = novo;
	}
	//Metodo toString para retornar as informa��es da classe
	@Override
	public String toString() {
		return datahora + " [" + status + "]";
	}
	
}
