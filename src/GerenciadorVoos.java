import java.time.LocalDate;
import java.util.Comparator;
import java.util.ArrayList;

public class GerenciadorVoos {

	//Declarando um Array de Voo 
	private ArrayList<Voo> voos;
	
	//Metodo construtor
	public GerenciadorVoos(){
		voos=new ArrayList<>();
	}
	
	//Metodo para oredenacao por hora
	public void ordenaDataHora(){
		voos.sort(Comparator.comparing(Voo::getDatahora));
	}
	
	//Metodo para ordenacao e desempata pela duracao
	public void ordenaDataHoraDuracao(){
		voos.sort(Comparator.comparing(Voo::getDatahora)
				.thenComparing(Voo::getDuracao));
	}
	
	//Adiciona um Voo
	public void adicionar(Voo v){
		voos.add(v);
	}
	
	//Cria um novo array de Voo e recebe o Array de voos
	public ArrayList<Voo> listarTodos(){
		return new ArrayList<Voo>(voos);
	}
	
//M�todo para buscar por data 	
	public ArrayList<Voo> buscarData(LocalDate data) {
		//Cria um novo Array de Voo com o nome de lista 
		ArrayList<Voo> lista = new ArrayList<>();
		//For para percorrer o Array de voos
		for(Voo v : voos) {
			//caso seja igual, sera armazenado no novo Array
			if(v.getDatahora().toLocalDate().equals(data))
				lista.add(v);							
		}
		//Retorna o Array de lista
		return lista; // "não achamos!"
	}
	
	
}
