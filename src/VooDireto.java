import java.time.Duration;
import java.time.LocalDateTime;


//Cria uma classe extendida de Voo para Voos diretos
public class VooDireto extends Voo {
	
	private Rota rota;
	
	//Construtor e Reutiliza o datahora da classe Voo utilizando super
	public VooDireto(LocalDateTime datahora, Rota rota) {
		super(datahora);
		this.rota = rota;
	}
	
	//Retorna a Rota
	@Override
	public Rota getRota() {
		return rota;
	}
	
	//Cria um m�todo para calcular a Duracao
	@Override
	public Duration getDuracao() {
		double dist = rota.getOrigem().getLocal()
				.distancia(rota.getDestino().getLocal());
		double dur = dist / 805 + 0.5;
		int minutos = (int) (dur * 60);
		return Duration.ofMinutes(minutos);
	}
}
